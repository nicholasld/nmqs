package cn.nicholasld.nmqs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@MapperScan("cn.nicholasld.nmqs.dao")
@EnableAsync
public class NmqsApplication {
    public static void main(String[] args) {
        SpringApplication.run(NmqsApplication.class, args);
        System.out.println("  _   _ __  __  ____   _____ \n" +
                            " | \\ | |  \\/  |/ __ \\ / ____|\n" +
                            " |  \\| | \\  / | |  | | (___  \n" +
                            " | . ` | |\\/| | |  | |\\___ \\ \n" +
                            " | |\\  | |  | | |__| |____) |\n" +
                            " |_| \\_|_|  |_|\\___\\_\\_____/ ");
        System.out.println("NicholasLD's Message Queue Server is running...");
    }

}
