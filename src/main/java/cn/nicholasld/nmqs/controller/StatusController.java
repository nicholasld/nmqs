package cn.nicholasld.nmqs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author NicholasLD
 * @createTime 2023/4/25 18:05
 */
@Controller
public class StatusController {
    class Status {
        private int code;
        private String status;

        public Status(int code, String status) {
            this.code = code;
            this.status = status;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    @RequestMapping("/ping")
    @ResponseBody
    @CrossOrigin(origins = "*", maxAge = 3600)
    public Object status() {
        return new Status(200, "pong");
    }
}
