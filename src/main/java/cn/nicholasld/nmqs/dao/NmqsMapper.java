package cn.nicholasld.nmqs.dao;

import cn.nicholasld.nmqs.model.MqttConnection;
import org.springframework.stereotype.Repository;

/**
 * @author NicholasLD
 * @createTime 2023/4/24 17:35
 */
@Repository
public interface NmqsMapper {
    MqttConnection getMqttConnectionByToken(String token);
}
