package cn.nicholasld.nmqs.model;

/**
 * @author NicholasLD
 * @createTime 2023/3/30 01:17
 */
public class MqttConnection {
    int protocol;
    private String server;
    private String port;
    private String username;
    private String password;

    public MqttConnection() {
    }

    public MqttConnection(int protocol, String server, String port, String username, String password) {
        this.protocol = protocol;
        this.server = server;
        this.port = port;
        this.username = username;
        this.password = password;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getProtocol() {
        return protocol;
    }

    public void setProtocol(int protocol) {
        this.protocol = protocol;
    }
}
