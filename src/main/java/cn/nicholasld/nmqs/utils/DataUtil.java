package cn.nicholasld.nmqs.utils;

import cn.nicholasld.nmqs.dao.NmqsMapper;
import cn.nicholasld.nmqs.model.MqttConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author NicholasLD
 * @createTime 2023/4/24 17:56
 */
@Component
public class DataUtil {
    private static NmqsMapper nmqsMapper;
    @Autowired
    public void setNmqsMapper(NmqsMapper nmqsMapper) {
        DataUtil.nmqsMapper = nmqsMapper;
    }

    public static MqttConnection getMqttConnectionByToken(String token) {
        return nmqsMapper.getMqttConnectionByToken(token);
    }

}
