# Nmqs - NicholasLD's Message Queue Service

### 项目介绍

本项目用于对接 [WeMQ项目](https://gitee.com/nicholasld/WeMQ)，单独使用无任何效果，并且本项目仅适用于学习参考，您也可以为本项目提交维护代码，如果有其他问题，欢迎提交Issue！

### 部署教程

在部署该项目前，请确保 [WeMQ项目](https://gitee.com/nicholasld/WeMQ) 已部署完毕并正常运行。

1. 修改`src/main/resources/application.yml`中的数据库配置信息，本项目与 [WeMQ项目](https://gitee.com/nicholasld/WeMQ) 共用一个数据库，暂不支持分开。
2. 按照SpringBoot项目正常启动即可。
